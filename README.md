# Assessment for frontend-engineer
This *assessment* is to test best pratices, coding standards, problem-solving, research & use of react. The project should be submitted within a week of receival and should be done in Nextjs. If you got into any sort of problem like time-limit please feel free to reach out with a appropriate reason. 

## Assessment
Create a blogging website where users can list, search, paginate posts and view a post in nextjs. Follow this website for reference https://exampirate.com/blogs.

## Feature Requirements
- A normal user should be able to view 10 posts at a time. (Add pagination)
- A user should be able to search posts. (search should be done with title only)
- When user clicks a post then the user should be able to view the contents of the post.
- An admin user can login to the website and create / update / delete a post. You can mimic the api by using local storage or [json-server](https://www.npmjs.com/package/json-server).
- When user hits authenticated routes then the user should be redirected to 401 page.
- Non existing routes should redirect to 404 page.
- Every page should have meta title & description.
- Also add translation feature that can switch between english & nepali. (optional would be extra points)

## Tech requirements
- Must create a document that explain the installation, usage etc.
- Add proper commits and after a week do a pull request on main.
- Must use next js effectively.
- Admin pages should be server-side rendering and user pages like /blogs & /blogs/post-slug should be incremental static rendering. The revalidation time can be 60s.
- Must use hooks & proper components.
- Must use eslint & prettier.
- Utility functions should have comments & follow jsdoc standard.
- Use prop-types / typescript.

## Resources
- For api's use [jsonplaceholder.typicode.com](https://jsonplaceholder.typicode.com/) or you can create your own with json-server.
- [hazesoft-react-boilerplate-guidelines](https://github.com/aomini/Haze-react-boilerplate) use this to get some practices idea.
- Static testing reference https://github.com/aomini/Static-testing

## What you should keep in mind??
- Write code at your best.
- Donot leave anything unattended.
- Remove unnecessary comments but add neccessary comments.
- Try the solve the problem with best posible solution you can find.

## Confidential
> This project details should be confidential. No man should speak about it except you. Every thing should be done by yourself & your research. Noone should help you to write your code as this will be easily spotted. If you fail to keep it to yourself then you won't be on our list. 

## Get Help
You can get help from the internet articles, blogs, Q&A's etc. And also regarding confusion on questions you can reach Rakesh Shrestha @rak3sh.shrestha@gmail.com.

